const fs = require('fs')
const ejs = require('ejs')


const dirTemplate = __dirname

function render(file, data) {
  const stringOfTemplate = fs.readFileSync(`${dirTemplate}/${file}`, 'utf8')
  const renderedTemplate = ejs.render(stringOfTemplate, data);

  return renderedTemplate
}

module.exports = render
