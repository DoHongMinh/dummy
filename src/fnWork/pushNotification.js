const moment = require('moment')
const _ = require('lodash')
const CacheNotificationPush = require('../models/CacheNotificationPush')
const NotifyAPI = require('../api/NotifyApi')
const AdminAPI = require('../api/AdminApi')
const FcmAPI = require('../api/FcmApi')

const WARNING_LEVELS = require('../constants/warningLevels')
const STATUS_STATION = require('../constants/stationStatus')
const LABELS = require('../constants/labels')
const NOTIFY = require('../constants/notification')
const { ENUM_STATUS_DEVICE } = require('../constants/statusDevice')
import { checkAndPushNotification } from './../api/MonitoringApi'

// NOTE: logic push noti được handle bởi service monitoring
export async function pushNotification({ organization, station, data }, timeNhan) {
  await checkAndPushNotification({ organization, station, data })
  // let isPush = _.result(station, 'options.warning.allowed', false)

  // // if (isPush) {
  // // const dataFrequency = _.result(station, 'dataFrequency', 0);
  // // isPush = moment().isBefore(moment(timeNhan).add(dataFrequency * 3, 'minutes'));
  // // }

  // if (isPush) {
  //   checkAndPushStatusDevice({ organization, station, data })
  //   checkAndPushExceed({ organization, station, data })
  // }
}

// NOTE  với mỗi thiết bị sẽ check và gửi cảnh báo cho từng thiết bị
async function checkAndPushStatusDevice({ organization, station, data }) {
  const measuringLogs = _.result(data, 'measuringLogs', {})
  _.mapKeys(measuringLogs, async function (item, measure) {
    if (item.statusDevice == ENUM_STATUS_DEVICE.SENSOR_ERROR) {
      let isPush = await _logicIsPushError(organization, station, measure, item)
      if (isPush) {
        const notifyContent = FcmAPI.getDataForNoti({
          type: NOTIFY.SENSOR_ERROR,
          station,
          plaintext: measure,
          rawData: data,
        })

        sendNotifyToChannels({ organization, station, notifyContent })
      }
    }

    if (item.statusDevice == ENUM_STATUS_DEVICE.SENSOR_GOOD) {
      let isPush = await _logicIsPushError(organization, station, measure, item)
      // MARK  GOOD k0 ban nua
      // if (isPush) {
      //   const notifyContent = FcmAPI.getDataForNoti({
      //     type: NOTIFY.SENSOR_GOOD,
      //     station,
      //     plaintext: measure,
      //   });

      //   sendNotifyToChannels({organization, station, notifyContent})
      // }
    }
  })
}

async function checkAndPushExceed({ organization, station, data }) {
  // find measure exceed và measure prepare exceed
  const measuringLogs = _.result(data, 'measuringLogs', {})
  const measuringList = _.result(station, 'measuringList', [])

  let exceedArr = []
  let prepareExceedArr = []
  let dataFilter = []
  let dataFilterUnit = []

  // lọc ra list exceed and prepareExceed
  _.mapKeys(measuringLogs, function (item, measure) {
    if (item.warningLevel === WARNING_LEVELS.EXCEEDED) {
      exceedArr.push({
        ...item,
        key: measure,
        conf: measuringList.find((mea) => mea.measuringDes === measure),
        unitStr: _getLimitText(item),
      })

      dataFilter.push(measure)
      dataFilterUnit.push(_getLimitText(item))
    }

    // EXCEEDED_PREPARING sẽ k0 nắm trong message
    // if (item.warningLevel === WARNING_LEVELS.EXCEEDED_PREPARING) {
    //   prepareExceedArr.push({
    //     ...item,
    //     key: measure,
    //     conf: measuringList.find(mea => mea.measuringDes === measure),
    //     unitStr: _getLimitText(item)
    //   });
    //   dataFilter.push(measure);
    //   dataFilterUnit.push(_getLimitText(item))
    // }
  })

  // if (exceedArr.length > 0 || prepareExceedArr.length > 0)  chuan bi vuot va vuot deu bắn
  if (exceedArr.length > 0) {
    // logic hien tai, chỉ vuợt mới bắn
    // NOTE  logic Push
    let isPush = await _logicIsPushExceeded(organization, station, dataFilter, exceedArr.length > 0)

    if (!isPush) return

    const notifyContent = FcmAPI.getDataForNoti({
      type: NOTIFY.DATA_EXCEEDED,
      station,
      exceedArr,
      prepareExceedArr,
      dataFilter,
      rawData: data,
    })

    /*  NOTE push notification */
    sendNotifyToChannels({ organization, station, notifyContent })
  }
}

async function _logicIsPushExceeded(organization, station, dataFilter, isExceeded) {
  try {
    let cacheNotiPush
    cacheNotiPush = await CacheNotificationPush.findOne({
      organizationId: organization._id,
      stationId: station._id,
      $or: [{ status: NOTIFY.DATA_EXCEEDED }, { status: NOTIFY.DATA_EXCEEDED_PREPARED }],
    })

    if (!cacheNotiPush) {
      // NOTE  isPush =true, sau đó save vào database
      await CacheNotificationPush.create({
        organizationId: organization._id,
        stationId: station._id,
        measureList: dataFilter,
        timePush: moment().toDate(),
        status: isExceeded ? NOTIFY.DATA_EXCEEDED : NOTIFY.DATA_EXCEEDED_PREPARED,
      })
      return true
    }

    // 2.	Chuyển từ trạng thái Chuẩn bị vượt sang Vượt
    if (cacheNotiPush.status === NOTIFY.DATA_EXCEEDED_PREPARED && isExceeded) {
      await CacheNotificationPush.findOneAndUpdate(
        { _id: cacheNotiPush._id },
        {
          $set: {
            timePush: moment().toDate(),
            measureList: _.union(cacheNotiPush.measureList, dataFilter),
            status: NOTIFY.DATA_EXCEEDED,
          },
        }
      )

      return true
    }

    // 3.	Tăng thêm chỉ tiêu vượt ngưỡng
    const measureMerge = _.union(cacheNotiPush.measureList, dataFilter)
    if (!_.isEqual(cacheNotiPush.measureList, measureMerge)) {
      let status = cacheNotiPush.status || isExceeded ? NOTIFY.DATA_EXCEEDED : NOTIFY.DATA_EXCEEDED_PREPARED

      await CacheNotificationPush.findOneAndUpdate(
        { _id: cacheNotiPush._id },
        {
          $set: {
            timePush: moment().toDate(),
            measureList: _.union(cacheNotiPush.measureList, dataFilter),
            status: status,
          },
        }
      )
      return true
    }

    return false
  } catch (e) {
    console.log('Lỗi _logicIsPushExceeded:', e)
  }
}

/**
 * @returns {boolean}
 * true = error
 * false = good
 * */

async function _logicIsPushError(organization, station, measure, device) {
  try {
    let cacheNotiPush = await CacheNotificationPush.findOne({
      organizationId: organization._id,
      stationId: station._id,
      $or: [{ status: NOTIFY.SENSOR_GOOD }, { status: NOTIFY.SENSOR_ERROR }],
      measure,
    })

    // console.log('cacheNotiPush', cacheNotiPush)

    if (!cacheNotiPush) {
      // NOTE nếu lần đầu GOOD thì k0 gửi
      if (device.statusDevice === ENUM_STATUS_DEVICE.SENSOR_GOOD) return false

      await CacheNotificationPush.create({
        organizationId: organization._id,
        stationId: station._id,
        status: NOTIFY.SENSOR_ERROR,
        measure,
      })
      return true
    }

    // NOTE cacheNotiPush.status: String, device.statusDevice: Number =>  so sánh giá trị
    if (cacheNotiPush.status != ENUM_STATUS_DEVICE[device.statusDevice]) {
      await CacheNotificationPush.findOneAndUpdate(
        { _id: cacheNotiPush._id },
        {
          $set: {
            status: ENUM_STATUS_DEVICE[device.statusDevice],
          },
        }
      )
      return true
    }

    return false
  } catch (e) {
    console.log('lỗi _logicIsPushError:', e)
    return false
  }
}

function _getLimitText(measure) {
  const { unit } = measure
  let minLimit = _.get(measure, 'minLimit', null)
  let maxLimit = _.get(measure, 'maxLimit', null)
  if (minLimit === '') minLimit = null
  if (maxLimit === '') maxLimit = null

  if (minLimit !== null && maxLimit !== null) {
    return `(${minLimit} -> ${maxLimit}${unit ? ' ' + unit : ''})`
  }

  if (minLimit !== null) {
    return `(> ${minLimit}${unit ? ' ' + unit : ''})`
  }

  if (maxLimit !== null) {
    return `(< ${maxLimit}${unit ? ' ' + unit : ''})`
  }

  return ``
}

async function sendNotifyToChannels({ organization, station, notifyContent }) {
  /* default luôn luôn gởi fcm, không cần role */
  sendFCM({ organization, station, notifyContent })

  /* phải có role warning mới gởi */
  if (!station.options.warning.allowed) return console.log(LABELS.APP, 'Station chưa enable warning'.blue)

  sendEmail({ organization, station, notifyContent })
  sendSMS({ organization, station, notifyContent })
}

async function sendFCM({ organization, station, notifyContent }) {
  await FcmAPI.addNotiByOrganizationApi(organization._id, notifyContent, station)
}

/* MARK */
async function sendEmail({ organization, station, notifyContent }) {
  return console.log('tam thoi khong goi email, tiet kiem chi phi')
  /* send to email */
  let emails = await AdminAPI.getValidEmailsOfStation(organization._id, station._id)
  if (emails.length !== 0) {
    await NotifyAPI.sendEmail(organization._id, emails, notifyContent)
  }
}

/* MARK */
async function sendSMS({ organization, station, notifyContent }) {
  return console.log('tam thoi khong goi sms, tiet kiem chi phi')
  /* send to sms */
  let phones = await AdminAPI.getValidSMSsOfStation(organization._id, station._id)
  if (phones.length !== 0) {
    let smsContent = `${notifyContent.title} ${notifyContent.status} ${notifyContent.full_body} at ${notifyContent.time}`
    await NotifyAPI.sendSMS(organization._id, phones[0].phoneNumber, smsContent)
  }
}

// module.exports.pushNotification = pushNotification
