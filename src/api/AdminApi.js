const config = require('../config');
const axios = require('axios');
const constNoti = require('../constants/notification');
const _ = require('lodash');
const { COLOR_STATUS } = require('../constants/statusDevice');
const LABELS = require('../constants/labels')

const tempExceed = `Các chỉ tiêu <span style="color: red;">Vượt ngưỡng:</span>COD 30 (20 mg/L), TSS 12 (10 mg/L)`;
const tempExceed_vn = `Các chỉ tiêu <span style="color: red;">Vượt ngưỡng:</span>COD 30 (20 mg/L), TSS 12 (10 mg/L)`;
const tempPrepareExceed = `Các chỉ tiêu <span style="color: red;">Vượt ngưỡng:</span>COD 30 (20 mg/L), TSS 12 (10 mg/L)`;
const tempPrepareExceed_vn = `Các chỉ tiêu <span style="color: red;">Vượt ngưỡng:</span>COD 30 (20 mg/L), TSS 12 (10 mg/L)`;

const getMessExceed = (ExceedArr = []) => {
  let str = `Parameters <span style="color: ${COLOR_STATUS.EXCEEDED};">Exceeded: </span>`;
  for (var i = 0; i < ExceedArr.length; i++) {
    const item = ExceedArr[i];
    str += `${item.key} ${item.value} ${item.unitStr}`;

    if (i < ExceedArr.length - 1) str += ', ';
  }
  return str;
};

const getMessPrepareExceed = (PrepareExceedArr = []) => {
  let str = `Parameters <span style="color: ${COLOR_STATUS.EXCEEDED_PREPARING};">Tend to Exceed: </span>`;
  for (var i = 0; i < PrepareExceedArr.length; i++) {
    const item = PrepareExceedArr[i];
    str += `${item.key} ${item.value} ${item.unitStr}`;

    if (i < PrepareExceedArr.length - 1) str += ', ';
  }
  return str;
};

const instance = axios.create({
  baseURL: config.FCM_API + '/fcm-notification/',
  headers: {
    'Content-Type': 'application/json',
    'secret-token': config.SECRET_TOKEN
  },
});

async function addNotiByOrganizationApi(organizationId, contentNoti, station) {
  try {
    const response = await instance.post(`/${organizationId}`, contentNoti);
    console.log(LABELS.APP, 'push notification thanh cong'.green)
    return response.data;
  } catch (error) {
    if (error.message) console.error(error.message);
    else console.error(error);
  }
}

// type notification
function getDataForNoti(
  type,
  { station, plaintext = '', dataFilter = [], exceedArr = [], prepareExceedArr = [], color }
) {
  let result = {};

  switch (type) {
    case constNoti.NOTIFICATION_ERROR:
      result = {
        title: station.stationName,
        short_body: `${plaintext}`,
        full_body: `<div><span style="color: ${color};">${plaintext}</span></div>`,
        type: type,
        station_id: station._id,
        dataFilter: dataFilter
      };
      break;

    case constNoti.NOTIFICATION_EXCEEDED:
      let short_body = `Data Over-range`;
      let full_body = '';
      if (exceedArr.length > 0) full_body += getMessExceed(exceedArr);
      if (full_body && prepareExceedArr.length > 0) full_body += "<div style='height: 4px'></div>";
      if (prepareExceedArr.length > 0) full_body += getMessPrepareExceed(prepareExceedArr);

      result = {
        title: station.stationName,
        short_body,
        full_body: `<div>${full_body}</div>`,
        type: type,
        station_id: station._id,
        dataFilter: dataFilter
      };
      break;

    case constNoti.NOTIFICATION_DATA_LOSS:
      result = {
        title: station.stationName,
        short_body: `${plaintext}`,
        full_body: `<div><span style="color: ${color};">${plaintext}</span></div>`,
        type: type,
        station_id: station._id,
        dataFilter: dataFilter
      };
      break;
    default:
      break;
  }
  return result;
}

module.exports = {
  addNotiByOrganizationApi,
  getDataForNoti
};
