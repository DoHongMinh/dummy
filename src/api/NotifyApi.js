const axios = require('axios')
const configs = require('../config')
const LABELS = require('../constants/labels')

const Notify = axios.create({
  baseURL: `${configs.GATEWAY_API_URL}/notify`
})

const renderTemplate = require('../templates')

async function sendEmail(organizationID, emails = [], data = {}) {
  try {
    await Notify.post('send-email', {
      type: 'email',
      organizationID,
      emails: [configs.NOTIFY_FROM], 
      bcc: emails,
      subject: 'ilotusland Notification',
      html: renderTemplate('index.ejs', data)
    })
    console.log(LABELS.APP, `Send email đến ${emails} thành công`.green)
  }
  catch(error) {
    console.log(LABELS.APP, 'Lỗi khi gửi mail'.red)
    console.log(LABELS.APP, error.message.red)
  }
}

async function sendSMS(organizationID, phone, content) {
  try {
    Notify.post('send-sms', {
      type: 'sms',
      organizationID,
      phone,
      content
    })
    console.log(LABELS.APP, `Send sms đến ${phone} thành công`.green)
  }
  catch(error) {
    console.log(LABELS.APP, 'Lỗi khi gửi SMS'.red)
  }
}

module.exports = {
  sendEmail,
  sendSMS
}