export default {
  ['PM10']: {
    min: 17,
    max: 50,
    status: 2,
  },
  ['PM2_5']: {
    min: 10,
    max: 30,
  },
  ['NO2']: {
    min: 4,
    max: 20,
  },
  ['NO']: {
    min: 3,
    max: 30,
  },
  ['NOx']: {
    min: 10,
    max: 50,
  },
  ['CO']: {
    min: 10,
    max: 1000,
  },
  ['SO2']: {
    min: 0.5,
    max: 10,
  },
  ['O3']: {
    min: 1,
    max: 10,
  },
  ['Temp']: {
    min: 20,
    max: 40,
  },
  ['Do_Am']: {
    min: 50,
    max: 60,
  },
  ['TSS']: {
    min: 10,
    max: 200,
  },
  ['FLOW']: {
    min: 10,
    max: 200,
  },
  ['Clo']: {
    min: 10,
    max: 200,
  },
  ['COD']: {
    min: 10,
    max: 200,
  },
  ['pH']: {
    min: 10,
    max: 200,
  },
  ['COLOR']: {
    min: 10,
    max: 200,
  },
  ['Cu']: {
    min: 10,
    max: 200,
  },
  ['Amoni']: {
    min: 10,
    max: 200,
  },
  ['Dust']: {
    min: 10,
    max: 200,
  },
  ['Dust']: {
    min: 10,
    max: 200,
  },
  ['TSP']: {
    min: 10,
    max: 200,
  },
  ['Fe']: {
    min: 10,
    max: 200,
  },
  ['Zn']: {
    min: 10,
    max: 200,
  },
}