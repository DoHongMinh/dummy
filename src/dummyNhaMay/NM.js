export default {
  ['NM_Ho_Tay']: {
    ['Temp']: {
      min: 20,
      max: 30,
      status: 2,
    },
    ['TSS']: {
      min: 10,
      max: 100,
    },
    ['pH']: {
      min: 7,
      max: 9.5,
    },
    ['Aldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['BHC']: {
      min: 0.05,
      max: 0.5,
    },
    ['Dieldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['DDTs']: {
      min: 0.5,
      max: 5,
      status: 1,
    },
    ['Heptachlor & Heptachlorepoxide']: {
      min: 0.05,
      max: 0.5,
    },
    ['As']: {
      min: 0.001,
      max: 0.5,
    },
    ['Cd']: {
      min: 0.001,
      max: 0.5,
    },

    ['Pb']: {
      min: 0.001,
      max: 1,
    },
    ['Cr6+']: {
      min: 0.001,
      max: 1,
    },
    ['Cu']: {
      min: 0.001,
      max: 5,
    },
    ['Zn']: {
      min: 0.05,
      max: 10,
    },
    ['Hg']: {
      min: 0.001,
      max: 1,
    },
    ['DO']: {
      min: 5,
      max: 15,
    },

    ['BOD']: {
      min: 3,
      max: 70,
    },
    ['COD']: {
      min: 5,
      max: 200,
    },
    ['TOC']: {
      min: 3,
      max: 100,
    },

    ['Amoni']: {
      min: 0.1,
      max: 10,
    },
    ['NO3']: {
      min: 5,
      max: 30,
    },
    ['NO2']: {
      min: 0.01,
      max: 5,
    },
    ['Photphates']: {
      min: 0.1,
      max: 5,
    },
    ['COLIFORM']: {
      min: 1000,
      max: 9000,
      status: 1
    },
    ['Ecoli']: {
      min: 10,
      max: 300,
    },
  },
  ['NM_Hoan_Kiem']: {
    ['Temp']: {
      min: 20,
      max: 30,
    },
    ['TSS']: {
      min: 10,
      max: 100,
    },
    ['pH']: {
      min: 7,
      max: 9.5,
    },
    ['Aldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['BHC']: {
      min: 0.05,
      max: 0.5,
    },
    ['Dieldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['DDTs']: {
      min: 0.5,
      max: 5,
    },
    ['Heptachlor & Heptachlorepoxide']: {
      min: 0.05,
      max: 0.5,
    },
    ['As']: {
      min: 0.001,
      max: 0.5,
    },
    ['Cd']: {
      min: 0.001,
      max: 0.5,
    },

    ['Pb']: {
      min: 0.001,
      max: 1,
    },
    ['Cr6+']: {
      min: 0.001,
      max: 1,
    },
    ['Cu']: {
      min: 0.001,
      max: 5,
    },
    ['Zn']: {
      min: 0.05,
      max: 10,
    },
    ['Hg']: {
      min: 0.001,
      max: 1,
    },
    ['DO']: {
      min: 5,
      max: 15,
    },

    ['BOD']: {
      min: 3,
      max: 70,
    },
    ['COD']: {
      min: 5,
      max: 200,
    },
    ['TOC']: {
      min: 3,
      max: 100,
    },

    ['Amoni']: {
      min: 0.1,
      max: 10,
    },
    ['NO3']: {
      min: 5,
      max: 30,
      status: 2
    },
    ['NO2']: {
      min: 0.01,
      max: 5,
    },
    ['Photphates']: {
      min: 0.1,
      max: 5,
    },
    ['COLIFORM']: {
      min: 1000,
      max: 9000,
    },
    ['Ecoli']: {
      min: 10,
      max: 300,
    },
  },
  ['NM_Song_Nhue']: {
    ['Temp']: {
      min: 20,
      max: 30,
    },
    ['TSS']: {
      min: 10,
      max: 100,
    },
    ['pH']: {
      min: 7,
      max: 9.5,
    },
    ['Aldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['BHC']: {
      min: 0.05,
      max: 0.5,
    },
    ['Dieldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['DDTs']: {
      min: 0.5,
      max: 5,
    },
    ['Heptachlor & Heptachlorepoxide']: {
      min: 0.05,
      max: 0.5,
    },
    ['As']: {
      min: 0.001,
      max: 0.5,
    },
    ['Cd']: {
      min: 0.001,
      max: 0.5,
    },

    ['Pb']: {
      min: 0.001,
      max: 1,
    },
    ['Cr6+']: {
      min: 0.001,
      max: 1,
    },
    ['Cu']: {
      min: 0.001,
      max: 5,
    },
    ['Zn']: {
      min: 0.05,
      max: 10,
    },
    ['Hg']: {
      min: 0.001,
      max: 1,
    },
    ['DO']: {
      min: 5,
      max: 15,
    },

    ['BOD']: {
      min: 3,
      max: 70,
    },
    ['COD']: {
      min: 5,
      max: 200,
    },
    ['TOC']: {
      min: 3,
      max: 100,
    },

    ['Amoni']: {
      min: 0.1,
      max: 10,
    },
    ['NO3']: {
      min: 5,
      max: 30,
    },
    ['NO2']: {
      min: 0.01,
      max: 5,
    },
    ['Photphates']: {
      min: 0.1,
      max: 5,
    },
    ['COLIFORM']: {
      min: 1000,
      max: 9000,
    },
    ['Ecoli']: {
      min: 10,
      max: 300,
    },
  },
  ['NM_Song_To_Lich']: {
    ['Temp']: {
      min: 20,
      max: 30,
    },
    ['TSS']: {
      min: 10,
      max: 100,
    },
    ['pH']: {
      min: 7,
      max: 9.5,
    },
    ['Aldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['BHC']: {
      min: 0.05,
      max: 0.5,
    },
    ['Dieldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['DDTs']: {
      min: 0.5,
      max: 5,
    },
    ['Heptachlor & Heptachlorepoxide']: {
      min: 0.05,
      max: 0.5,
    },
    ['As']: {
      min: 0.001,
      max: 0.5,
    },
    ['Cd']: {
      min: 0.001,
      max: 0.5,
    },

    ['Pb']: {
      min: 0.001,
      max: 1,
    },
    ['Cr6+']: {
      min: 0.001,
      max: 1,
    },
    ['Cu']: {
      min: 0.001,
      max: 5,
    },
    ['Zn']: {
      min: 0.05,
      max: 10,
    },
    ['Hg']: {
      min: 0.001,
      max: 1,
    },
    ['DO']: {
      min: 5,
      max: 15,
    },

    ['BOD']: {
      min: 3,
      max: 70,
    },
    ['COD']: {
      min: 5,
      max: 200,
    },
    ['TOC']: {
      min: 3,
      max: 100,
    },

    ['Amoni']: {
      min: 0.1,
      max: 10,
    },
    ['NO3']: {
      min: 5,
      max: 30,
    },
    ['NO2']: {
      min: 0.01,
      max: 5,
    },
    ['Photphates']: {
      min: 0.1,
      max: 5,
    },
    ['COLIFORM']: {
      min: 1000,
      max: 9000,
    },
    ['Ecoli']: {
      min: 10,
      max: 300,
    },
  },
  ['NMSuoiLaiSon']: {
    ['Temp']: {
      min: 20,
      max: 30,
    },
    ['TSS']: {
      min: 10,
      max: 100,
    },
    ['pH']: {
      min: 7,
      max: 9.5,
      status: 2
    },
    ['Aldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['BHC']: {
      min: 0.05,
      max: 0.5,
      status: 1
    },
    ['Dieldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['DDTs']: {
      min: 0.5,
      max: 5,
    },
    ['Heptachlor & Heptachlorepoxide']: {
      min: 0.05,
      max: 0.5,
    },
    ['As']: {
      min: 0.001,
      max: 0.5,
    },
    ['Cd']: {
      min: 0.001,
      max: 0.5,
    },

    ['Pb']: {
      min: 0.001,
      max: 1,
    },
    ['Cr6+']: {
      min: 0.001,
      max: 1,
    },
    ['Cu']: {
      min: 0.001,
      max: 5,
    },
    ['Zn']: {
      min: 0.05,
      max: 10,
    },
    ['Hg']: {
      min: 0.001,
      max: 1,
    },
    ['DO']: {
      min: 5,
      max: 15,
    },

    ['BOD']: {
      min: 3,
      max: 70,
    },
    ['COD']: {
      min: 5,
      max: 200,
    },
    ['TOC']: {
      min: 3,
      max: 100,
    },

    ['Amoni']: {
      min: 0.1,
      max: 10,
    },
    ['NO3']: {
      min: 5,
      max: 30,
    },
    ['NO2']: {
      min: 0.01,
      max: 5,
    },
    ['Photphates']: {
      min: 0.1,
      max: 5,
    },
    ['COLIFORM']: {
      min: 1000,
      max: 9000,
    },
    ['Ecoli']: {
      min: 10,
      max: 300,
    },
  },
  ['NM_Song_CauBay']: {
    ['Temp']: {
      min: 20,
      max: 30,
    },
    ['TSS']: {
      min: 10,
      max: 100,
    },
    ['pH']: {
      min: 7,
      max: 9.5,
    },
    ['Aldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['BHC']: {
      min: 0.05,
      max: 0.5,
    },
    ['Dieldrin']: {
      min: 0.05,
      max: 0.5,
    },
    ['DDTs']: {
      min: 0.5,
      max: 5,
    },
    ['Heptachlor & Heptachlorepoxide']: {
      min: 0.05,
      max: 0.5,
    },
    ['As']: {
      min: 0.001,
      max: 0.5,
    },
    ['Cd']: {
      min: 0.001,
      max: 0.5,
    },

    ['Pb']: {
      min: 0.001,
      max: 1,
    },
    ['Cr6+']: {
      min: 0.001,
      max: 1,
    },
    ['Cu']: {
      min: 0.001,
      max: 5,
    },
    ['Zn']: {
      min: 0.05,
      max: 10,
    },
    ['Hg']: {
      min: 0.001,
      max: 1,
    },
    ['DO']: {
      min: 5,
      max: 15,
    },

    ['BOD']: {
      min: 3,
      max: 70,
    },
    ['COD']: {
      min: 5,
      max: 200,
    },
    ['TOC']: {
      min: 3,
      max: 100,
    },

    ['Amoni']: {
      min: 0.1,
      max: 10,
    },
    ['NO3']: {
      min: 5,
      max: 30,
    },
    ['NO2']: {
      min: 0.01,
      max: 5,
    },
    ['Photphates']: {
      min: 0.1,
      max: 5,
    },
    ['COLIFORM']: {
      min: 1000,
      max: 9000,
    },
    ['Ecoli']: {
      min: 10,
      max: 300,
    },
  },
}
