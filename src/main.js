import mongoose from 'mongoose'
import express from 'express'
import { DB, DB_ORGANIZATION_NAME } from './config'
import StationAutoModel from './models/StationAuto'
import DUMMY_NHAMAY from './dummyNhaMay'
import lamviec from './fnWork'
import OrganizationModel from './models/Organization'
import schedule from 'node-schedule'
import { DEFAULT_DUMMY_FREQUENCY } from './config'
const server = express()

mongoose.connect(DB.uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  user: DB.user,
  pass: DB.pass
})

const db = mongoose.connection

db.on('error', err => {
  console.error(err)
  process.exit(1)
})

db.once('open', async () => {
  // server.listen(60003)
  // console.log(`Process is listening on port 60003`)

  server.listen(60008)
  console.log(`Process is listening on port 60008`)
  //   const stationData = await StationAutoModel.find({})
  const org = await OrganizationModel.findOne({
    "databaseInfo.name": DB_ORGANIZATION_NAME
  })
  if (!org) {
    return console.log('========k0 tim thay database to chuc, vui long kiem tra lai cau hinh=====')
  }
  //   console.log(data)
  //  console.log('DUMMY_NHAMAY',DUMMY_NHAMAY)
  lamviec(org)
  schedule.scheduleJob(`*/${DEFAULT_DUMMY_FREQUENCY} * * * *`, function () {
    lamviec(org)
  });
})
