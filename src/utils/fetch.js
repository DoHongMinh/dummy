const axios = require('axios')

const getHeaders = token => {
    let headers = {
        Accept: 'application/json'
    }
    if (token) headers.Authorization = token
    return headers
}

function putFetch(url, data, props, token) {
    let attributes = Object.assign(
        {
            cache: true,
            headers: getHeaders(token)
        },
        props
    )

    return new Promise((resolve, reject) => {
        axios
            .put(url, data, attributes)
            .then(res => {
                if (res.status === 200) {
                    resolve(res.data)
                } else {
                    resolve({ error: true })
                }
            })
            .catch(e => reject(e))
    })
}

function postFetch(url, params, header = {}, token) {
    const headers = Object.assign(getHeaders(token), header)
    return new Promise((resolve, reject) => {
        axios
            .post(url, params, {
                cache: true,
                headers
            })
            .then(res => {
                if (res.status === 200) {
                    resolve(res.data)
                } else {
                    resolve({ error: true })
                }
            })
            .catch(e => reject(e))
    })
}

function getFetch(url, data, props, token) {
    let attributes = Object.assign(
        {
            headers: getHeaders(token)
        },
        props
    )
    return new Promise((resolve, reject) => {
        axios
            .get(url, attributes)
            .then(res => {
                if (res.status === 200) {
                    resolve(res.data)
                } else {
                    resolve({ error: true })
                }
            })
            .catch(e => reject(e))
    })
}
module.exports = { getFetch, putFetch, postFetch }